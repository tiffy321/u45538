#include <windows.h>
#include <TlHelp32.h> 
#include <iostream>
#include <string>
#include <tchar.h>
#include <psapi.h>
#include "mem.h"
#include "User.h"
#include "Mover.h"
#include "cfg.h"
#include <sstream>
#include "VersionCommon.h"
#include <vector>
#include <fstream>


using namespace std;


DWORD pid, dwBaseAddress, aProcesses[1024], cbNeeded, cProcesses; //create Client list with PayerName
string arCharacterDisplay[20], szCharacterName; //create Client list with PayerName
int state = 0; // breakpoint botting loop. (stop / play )

int pChar; // Character BasePtr
int nUserInput; // cin
int arClients[10]; // client array
int a = 1;		//counter
	

int main()
{
	
#ifdef __TESTSERVER
#endif // __TESTSERVER	
	
	
	cfg cfg;
	cfg.WriteConfig();


	


#ifdef __CONFIG_DEBUG
	cout << cfg::dwCharacterPtr << endl;
	cout << cfg::dwFirstSpawnPtr << endl;
	cout << cfg::dwPlayerTargetPtr << endl;
	cout << cfg::dwLastChatLinePtr << endl;
	cout << cfg::Name << endl;
	cout << cfg::Hp << endl;
	cout << cfg::CoordX << endl;
	cout << cfg::CoordZ << endl;
	cout << cfg::CoordY << endl;
	cout << cfg::Lv << endl;
	cout << cfg::Define << endl;
	cout << cfg::Activ << endl;
	cout << cfg::CharacterState << endl;
	cout << cfg::Target << endl;
	cout << cfg::MaxRange << endl;
#endif // DEBUG
	
	


	

	


	//CLIENT CONFIG 

	if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
	{
		return 1;
		//cout << sizeof(aProcesses);
	}
	
	cProcesses = cbNeeded / sizeof(DWORD);// Calculate how many process identifiers were returned.
	
	
	

	
	for (int i = 0; i < cProcesses; i++)
	{
		if (aProcesses[i] != 0)
		{

			if (PrintProcessNameAndID(aProcesses[i]) > 2)
			{	
				arClients[a] = PrintProcessNameAndID(aProcesses[i]);
				
				HANDLE pHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, arClients[a]);
				dwBaseAddress = dwGetModuleBaseAddress(arClients[a], "Neuz.exe");
				ReadProcessMemory(pHandle, (LPVOID)(dwBaseAddress + cfg::dwCharacterPtr), &pChar, sizeof(pChar), 0);
				ReadProcessMemory(pHandle, (LPVOID)(pChar + cfg::Name), &szCharacterName, sizeof(szCharacterName), 0);				
				

				int nStrLength = strlen(szCharacterName.c_str()); 
				
				if (nStrLength != 0)
				{
					printf("%d:  %s   pid:%d\n",a, szCharacterName.c_str(), arClients[a]);
					arCharacterDisplay[a] = szCharacterName.c_str();
					szCharacterName.clear();					
				}
				else
				{
					printf("%d:  <unknown>   pid:%d\n", a, arClients[a]);
					arCharacterDisplay[a] = "<unknown>";
				}

				a++;
			}
					
			
		}
	}
	
	
	cout << "------------" << endl;
	cout << "Enter a Client Number" << endl;

	cin >> nUserInput;
	while (true)
	{ 
		
		cin.clear();
		//cin.ignore(1000, '\n');
		if (nUserInput < 1 || nUserInput >= a)
		{
			cout << "Invalid ClientNumber, try again." << endl;
			cin.ignore(1000, '\n');
			cin >> nUserInput;
			
		}
		else
		{
			system("cls");
			cout << "Using: "<< arCharacterDisplay[nUserInput].c_str() << "@" << arClients[nUserInput] <<endl;
			pid = arClients[nUserInput];
			break;
		
		}
		
	}
	
	//Set WorkingHandle and Collect BaseData
	//create obj
	
	
	
	
	pUser pUser;
	pUser.pHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	pUser.dwBaseAddress = dwGetModuleBaseAddress(pid, "Neuz.exe");
	pUser.pid = pid;
	//pUser.dwCharacterPtr = 0x936C94;	
	//pUser.dwCharacterTargetPtr = 0x843BA4;	
	//pUser.dwLastChatline = 0x837530;

	pUser.dwCharacterPtr = cfg.dwCharacterPtr;
	pUser.dwCharacterTargetPtr = cfg.dwPlayerTargetPtr;
	pUser.dwLastChatline = cfg.dwLastChatLinePtr;

	


#ifdef __DEBUG
	cout << "HANDLE: " << pUser.pHandle << endl;
	cout << "ProcessID: " << pUser.pid << endl;
	cout << "ImageBase: " << pUser.dwBaseAddress << endl;
	cout << "Character: "<< pUser.GetName() << endl;
#endif //Debug
	
	//END config
	
	
	
	
	//START Select loop
	int x = 0; // MessageHandler Helper
	int  nCurrentMobLv;
	
	pUser.arModifyTarget(1);
	
	pMover pMover;
	while (true)
	{	
		
		
		if (state == 0)
		{	
			
			if (x == 0){cout << "Waiting..Go and click a Monster..."<< endl;}
			x = 1;

			
			if (pUser.arModifyTarget(0)[0] != 0) // listener
			{
				pUser.SetCenterPoint();
				nCurrentMobLv = pUser.arModifyTarget(0)[1];
				state = 2;
				x = 0;
			}
			


		}

		if (state == 1) // helper
		{
			pUser.arModifyTarget(1); //clear target
			state = 0;
			x = 0;
			system("cls");

		}
		
		if (state == 2)	//Select Loop	
		{	
			state = MessageHandler(pUser.pHandle, pUser.dwBaseAddress, pUser.dwLastChatline, pUser.GetName()); //command listener
				


			if (x == 0) 
			{ 
				system("cls"); // clear console
				cout << "SYSTEM: Set target Level to "<< pUser.arModifyTarget(0)[1] << endl; 
				cout << "INFO: Type '<3' into [General Chat] to reset." << endl;
			}
			x = 1;
			

			if (pUser.arModifyTarget(0)[0] < 1) // true if empty
			{
				
				int mob = arGetNewMob_v2(pUser.pHandle, cfg.dwFirstSpawnPtr, nCurrentMobLv, pUser.dwCharacterPtr, pUser.dwBaseAddress);
				
				
		
					#ifdef __DEBUG
					//cout << "Selected MoverLv: " << pMover.MoverLv <<endl;
					#endif			 
					pUser.arModifyTarget(1,pMover.MoverID);	
				

				
				
				

			}
		}


		//TODO clear listener
		/*
			if mob is unreachable or stolen
			{
				do something..
			}

		*/
	}

	

	
	

	
}


