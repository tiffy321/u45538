#define __CFG_H_



struct cfg
{
public:
	
	static int dwCharacterPtr;
	static int dwFirstSpawnPtr; 
	static int dwPlayerTargetPtr;
	static int dwLastChatLinePtr;

	static int Name;
	static int Hp;
	static int CoordX;
	static int CoordZ;
	static int CoordY;
	static int Lv;
	static int Define;
	static int Activ;
	static int CharacterState;
	static int Target;
	static int MaxRange;


	int WriteConfig();


};

