#include <windows.h>
#include <TlHelp32.h> 
#include <iostream>
#include <string>
#include <stdlib.h>  
#include "VersionCommon.h"
#include <stdio.h>
#include "User.h"
#include "cfg.h"

float pUser::fCenterPointX;
float pUser::fCenterPointY;
int pUser::LastMob;

int pUser::Util_CharacterBaseOFFSET()
{
	int _pChar;
	ReadProcessMemory(pHandle, (LPVOID)(dwBaseAddress + dwCharacterPtr), &_pChar, sizeof(_pChar), 0);
	return _pChar;
}

float pUser::SetCenterPoint()
{
	ReadProcessMemory(pHandle, (LPVOID)(pUser::Util_CharacterBaseOFFSET() + cfg::CoordX), &fCenterPointX, sizeof(fCenterPointX), 0);
	ReadProcessMemory(pHandle, (LPVOID)(pUser::Util_CharacterBaseOFFSET() + cfg::CoordY), &fCenterPointY, sizeof(fCenterPointY), 0);
	return 0;
}

BOOL pUser::nCharacterIsActiv()
{
	int _Buffer;
	ReadProcessMemory(pHandle, (LPVOID)(pUser::Util_CharacterBaseOFFSET() + cfg::Activ), &_Buffer, sizeof(_Buffer), 0);
	return _Buffer != 0 ? true : false;

}

int* pUser::arModifyTarget(int controller, int nMobID)
{
	int _nMob[2];
	int _nBuffer;
	int _nTargetID;
	int _TargetPtr;
	int _nTargetLv;

	ReadProcessMemory(pHandle, (LPVOID)(dwBaseAddress + dwCharacterTargetPtr), &_TargetPtr, sizeof(_TargetPtr), 0);

	if (controller == 0)
	{
		//std::cout << "0" << std::endl;
		ReadProcessMemory(pHandle, (LPVOID)(_TargetPtr + cfg::Target), &_nTargetID, sizeof(_nTargetID), 0);
		ReadProcessMemory(pHandle, (LPVOID)(_nTargetID + cfg::Lv), &_nTargetLv, sizeof(_nTargetLv), 0);
	}
	else if (controller == 1)
	{
		//std::cout << "1" << std::endl;
		_nBuffer = nMobID;
		WriteProcessMemory(pHandle, (LPVOID)(_TargetPtr + cfg::Target), &_nBuffer, sizeof(_nBuffer), 0);
		pUser::LastMob = nMobID;
	}

	_nMob[0] = _nTargetID;
	_nMob[1] = _nTargetLv;

	return _nMob;




}

std::string pUser::GetName()
{
	
	ReadProcessMemory(pHandle, (LPVOID)(pUser::Util_CharacterBaseOFFSET() + cfg::Name), &szCharacterName, sizeof(szCharacterName), 0);
	return szCharacterName.c_str();

}

