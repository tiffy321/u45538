#include "cfg.h"
#include <iostream>
#include <sstream>
#include <windows.h>
#include <tchar.h>
#include <string>
#include <fstream>
#include <psapi.h>

TCHAR path[MAX_PATH];
TCHAR FullPath = GetFullPathName(_T("foobar.ini"), MAX_PATH, path, NULL);

int cfg::dwCharacterPtr = GetPrivateProfileInt("POINTER", "dwCharacterPtr", 0, path);
int cfg::dwFirstSpawnPtr = GetPrivateProfileInt("POINTER", "dwFirstSpawnPtr", 0, path);
int cfg::dwPlayerTargetPtr = GetPrivateProfileInt("POINTER", "dwPlayerTargetPtr", 0, path);
int cfg::dwLastChatLinePtr = GetPrivateProfileInt("POINTER", "dwLastChatLinePtr", 0, path);
//int cfg::dwCameraRotatePtr = GetPrivateProfileInt("POINTER", "dwCameraRotatePtr", 0, path);

int cfg::Name = GetPrivateProfileInt("OFFSET", "Name", 0, path);
int cfg::Hp = GetPrivateProfileInt("OFFSET", "Hp", 0, path);
int cfg::CoordX = GetPrivateProfileInt("OFFSET", "CoordX", 0, path);
int cfg::CoordZ = GetPrivateProfileInt("OFFSET", "CoordZ", 0, path);
int cfg::CoordY = GetPrivateProfileInt("OFFSET", "CoordY", 0, path);
int cfg::Lv = GetPrivateProfileInt("OFFSET", "Lv", 0, path);
int cfg::Define = GetPrivateProfileInt("OFFSET", "Define", 0, path);
int cfg::Activ = GetPrivateProfileInt("OFFSET", "Activ", 0, path);
int cfg::CharacterState = GetPrivateProfileInt("OFFSET", "CharacterState", 0, path);
int cfg::Target = GetPrivateProfileInt("OFFSET", "Target", 0, path);
int cfg::MaxRange = GetPrivateProfileInt("FEATURE", "MaxRange", 0, path);
 


int cfg::WriteConfig()
{
	
	if (std::fstream{ "foobar.ini" });
	else
	{
		std::cerr << "SYSTEM: Creating config.ini\n";
		std::cerr << "SYSTEM: U've to update the ptr/offset values to run this bot\n";
		std::cerr << "SYSTEM: Press enter to confirm.\n";
		
	
	
		TCHAR path[MAX_PATH];
		TCHAR FullPath = GetFullPathName(_T("foobar.ini"), MAX_PATH, path, NULL);
	
	
		WritePrivateProfileString("POINTER", "dwCharacterPtr", "0x936C94", path);
		WritePrivateProfileString("POINTER", "dwFirstSpawnPtr", "0x9BC940", path);
		WritePrivateProfileString("POINTER", "dwPlayerTargetPtr", "0x843BA4", path);
		WritePrivateProfileString("POINTER", "dwCameraRotatePtr", "0xFFFFFF", path);
		WritePrivateProfileString("POINTER", "dwLastChatLinePtr", "0x837530", path);

		
		WritePrivateProfileString("OFFSET", "Name", "0x130c", path);
		WritePrivateProfileString("OFFSET", "Hp", "0x6b8", path);
		WritePrivateProfileString("OFFSET", "CoordX", "0x54", path);
		WritePrivateProfileString("OFFSET", "CoordZ", "0x58", path);
		WritePrivateProfileString("OFFSET", "CoordY", "0x5c", path);
		WritePrivateProfileString("OFFSET", "Lv", "0x698", path);

		
		WritePrivateProfileString("OFFSET", "## DEFINE RTRN VALUES 18 "," MONSTERS 2 = PLAYERS ##", path);
		WritePrivateProfileString("OFFSET", "Define", "0x4", path);
		
		WritePrivateProfileString("OFFSET", "## ACTIV RTRN VALUES [DOUBLE] MONSTER IDLE ", " 0.0078125, 2 = MONSTER IS FIGHTING ##", path);
		WritePrivateProfileString("OFFSET", "Activ", "0x794", path); 

		WritePrivateProfileString("OFFSET", "## STATE MEELE ", " 5 SKILL USE = 7 ##", path);
		WritePrivateProfileString("OFFSET", "CharacterState", "0x3e4", path);
	
		WritePrivateProfileString("OFFSET", "## EXAMPLE TARGET_PTR + 0x20 + OFFSET ", " HP/LV/X/Y/Z.. ##", path);
		WritePrivateProfileString("OFFSET", "Target", "0x20", path);

		WritePrivateProfileString("FEATURE", "MaxRange", "80", path);
		
		std::cin.get();
	}
	return 0;
}

	

