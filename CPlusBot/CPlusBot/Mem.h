#define	__MEM_H__


HANDLE GetProcessHandle(const char* neuzexe);

DWORD dwGetModuleBaseAddress(DWORD, const char* neuzexe);

int PrintProcessNameAndID(DWORD processID);

int arGetNewMob_v2(HANDLE pHandle, DWORD pMonsters, int nMobHp, DWORD dwChar, DWORD dwBaseAddress);


int MessageHandler(HANDLE pHandle, DWORD dwBaseAddress, DWORD dwLastChatLine, std::string szCharacterName);

