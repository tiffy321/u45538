#define __USER_H__


class pUser
{
	std::string szCharacterName;
	
	int nCharacterHp; //unused
	
	int nCharacterTargetID;
	int nCharacterTargetHP;
	int nCharacterTargetLV;

public:
	
	static float fCenterPointX; 
	static float fCenterPointY; 
	static int LastMob;
	
	DWORD dwCharacterPtr;
	DWORD dwCharacterTargetPtr;
	DWORD dwLastChatline;
	HANDLE pHandle;
	DWORD dwBaseAddress;
	int pid;
	int Util_CharacterBaseOFFSET();
	std::string GetName();
	int* arModifyTarget(int controller, int optional = 0);
	BOOL nCharacterIsActiv();
	float SetCenterPoint();
	

};