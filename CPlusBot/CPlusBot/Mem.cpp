#include <windows.h>
#include <TlHelp32.h> 
#include <iostream>
#include <string>
#include <psapi.h>
#include <stdlib.h>  
#include "Mem.h"
#include "Mover.h"
#include "User.h"
#include "cfg.h"
#include <math.h> 
#include <iomanip>
#include "VersionCommon.h"
#include <stdio.h>
#include <errno.h>

HANDLE GetProcessHandle(const char *procName)
{
	HANDLE hProc = NULL;
	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (Process32First(hSnapshot, &pe32)) {
		do {
			if (!strcmp(pe32.szExeFile, procName)) {
				hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
				break;
			}
		} while (Process32Next(hSnapshot, &pe32));
	}
	CloseHandle(hSnapshot);
	return hProc;
}


DWORD dwGetModuleBaseAddress(DWORD dwProcessIdentifier, const char *lpszModuleName)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessIdentifier);
	DWORD dwModuleBaseAddress = 0;
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		MODULEENTRY32 ModuleEntry32 = { 0 };
		ModuleEntry32.dwSize = sizeof(MODULEENTRY32);
		if (Module32First(hSnapshot, &ModuleEntry32))
		{
			do
			{
				if (strcmp(ModuleEntry32.szModule, lpszModuleName) == 0)
				{
					
					dwModuleBaseAddress = (DWORD)ModuleEntry32.modBaseAddr;
					break;
				}
			} while (Module32Next(hSnapshot, &ModuleEntry32));
		}
		CloseHandle(hSnapshot);
	}
	return dwModuleBaseAddress;
}

// generate neuz.exe list...

int PrintProcessNameAndID(DWORD processID)
{
	
	TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");

	// Get a handle to the process.

	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processID);

	// Get the process name.

	if (NULL != hProcess)
	{
		HMODULE hMod;
		DWORD cbNeeded;

		if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
		{
			GetModuleBaseName(hProcess, hMod, szProcessName, sizeof(szProcessName) / sizeof(TCHAR));
			if (lstrcmp(szProcessName, "Neuz.exe") == 0)
			{
				return processID;
				
			}


		}

		



	}


	CloseHandle(hProcess);
}



int arGetNewMob_v2(HANDLE pHandle, DWORD pMonsters, int nMobLv, DWORD dwChar, DWORD dwBaseAddress)
{
	int  arMob[225];
	DWORD  _dwStartAddress = pMonsters;

	//TestServer -> dwStartAddress: 0x9BC93c
	for (int i = 0; i < 225; i++)
	{
		_dwStartAddress = _dwStartAddress + 4;
		arMob[i] = _dwStartAddress;

	}


	
	int _nMaxDistance = 1000;
	//Character Information
	int _pChar;
	float _fPlayerx;
	float _fPlayery;
	
	ReadProcessMemory(pHandle, (LPVOID)(dwBaseAddress + dwChar), &_pChar, sizeof(_pChar), 0);
	ReadProcessMemory(pHandle, (LPVOID)(_pChar + cfg::CoordX), &_fPlayerx, sizeof(_fPlayerx), 0);
	ReadProcessMemory(pHandle, (LPVOID)(_pChar +cfg::CoordY), &_fPlayery, sizeof(_fPlayery), 0);


	//Monster Information
	std::string _szMobName;
	int _nMobID; 
	int _nMobHP;
	int _nMobLv;
	float _fMobx;
	float _fMoby;	
	int _nMobDefine;		//NPC 18 Player 2
	double _nMobActiv;		//default 0
	float  _DistanceToChar;

	for (int i = 0; i < 224;i++)
	{
		
		ReadProcessMemory(pHandle, (LPVOID)(dwBaseAddress + arMob[i]), &_nMobID, sizeof(_nMobID), 0);		 // Base ID
		ReadProcessMemory(pHandle, (LPVOID)(_nMobID + cfg::Hp), &_nMobHP, sizeof(_nMobHP), 0);				 // HP
		//ReadProcessMemory(pHandle, (LPVOID)(_nMobID + 0x130c), &_szMobName, sizeof(_szMobName), 0);			 // Name
		ReadProcessMemory(pHandle, (LPVOID)(_nMobID + cfg::CoordX), &_fMobx, sizeof(_fMobx), 0);					 // Coord X
		ReadProcessMemory(pHandle, (LPVOID)(_nMobID + cfg::CoordY), &_fMoby, sizeof(_fMoby), 0);				     // Coord Z
		ReadProcessMemory(pHandle, (LPVOID)(_nMobID + cfg::Lv), &_nMobLv, sizeof(_nMobLv), 0);				 // lvl
		ReadProcessMemory(pHandle, (LPVOID)(_nMobID + cfg::Define), &_nMobDefine, sizeof(_nMobDefine), 0);			 // must be 18 if u want to fight with Monsters
		ReadProcessMemory(pHandle, (LPVOID)(_nMobID + cfg::Activ), &_nMobActiv, sizeof(_nMobActiv), 0);			 // idle = .0078125  activ = 2

		//Some CrystalMath  nearest mob to Char
		float _distancex = (_fPlayerx - _fMobx) * (_fPlayerx - _fMobx);
		float _distancey = (_fPlayery - _fMoby) * (_fPlayery - _fMoby);
		_DistanceToChar = sqrt(_distancex + _distancey);

		//Some CrystalMath  max range
		float _CenterX = (pUser::fCenterPointX - _fMobx) * (pUser::fCenterPointX  - _fMobx);
		float _CenterY = (pUser::fCenterPointY - _fMoby) * (pUser::fCenterPointY - _fMoby);
		float _DistanceToCenter = sqrt(_CenterX + _CenterY);

		
		
		
		//std::cout << _nMobLv << std::endl;
		//Sleep(250);
		//if (_nMobHP == nMobHp)
		if (_nMobHP > 100 &&
			_nMobLv == nMobLv &&
			_nMobDefine == 18 
			
		
			#ifdef __ADVANCED_SELECT_ACTIV
			&& _nMobActiv == 0.0078125
			#endif // __ADVANCED_SELECT
			
			#ifdef __ADVANCED_SELECT_RANGE
			&& _DistanceToCenter < cfg::MaxRange
			#endif // __ADVANCED_SELECT_RANGE

			)
		{
			
			// Calculate nearest Monster to Character
			if (_DistanceToChar < _nMaxDistance)
			{
				_nMaxDistance = _DistanceToChar;
				
				//pMover::MoverName = _szMobName.c_str();
				
			
			
					pMover::MoverID = _nMobID;
					pMover::MoverLv = _nMobLv;
					pMover::MoverHp = _nMobHP;
					pMover::MoverDefine = _nMobDefine;
					pMover::DistanceToCenter = _DistanceToCenter;
					pMover::MoverActiv = _nMobActiv;

				
				
				

				
			}



		}

		
		
		
	}
	
	//crash fix. LastMob
	if (pMover::MoverID == pUser::LastMob)
	{
		pMover::MoverID = 0;
	}

	#ifdef __DEBUG
		system("cls");
		//std::cout << "PlayerID:  " << _pChar << std::endl;
		std::cout << "Player x: " << _fPlayerx << std::endl;
		std::cout << "Player y: " << _fPlayery << std::endl;
		std::cout << "LastMobID:  " << pUser::LastMob << std::endl;
		std::cout << "CurrentID:  " << pMover::MoverID << std::endl;
		std::cout << "MoverHP:  " << pMover::MoverHp << std::endl;
		std::cout << "MoverDefine:  " << pMover::MoverDefine << std::endl;
		//std::cout << "Distance " << _DistanceToChar << std::endl;
		std::cout << "MoverActiv:  " << pMover::MoverActiv << std::endl;
		std::cout << "MoverDistanceToCenter:  " << pMover::DistanceToCenter << std::endl;
	#endif //debug 		

	
	return 0;

}



int MessageHandler(HANDLE pHandle, DWORD dwBaseAddress, DWORD dwLastChatLine, std::string szCharacterName)
{
	char _msg[50];
	ReadProcessMemory(pHandle, (LPVOID)(dwBaseAddress + dwLastChatLine), &_msg, sizeof(_msg), 0);


	std::string _Stop = szCharacterName.c_str();
				_Stop += " : <3";


	
	//std::cout << _Stop << std::endl;
	if (_Stop.c_str() == (std::string)_msg)
	{
		
		int buffer = -1;
		WriteProcessMemory(pHandle, (LPVOID)(dwBaseAddress + dwLastChatLine), &buffer, sizeof(buffer), 0);
		
		
		return 1;
	}
	else
	{
		return 2;
	}
	

}


